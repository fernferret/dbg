FROM debian:buster

LABEL maintainer="fernferret"

RUN apt-get update && \
  apt-get install -y --no-install-recommends \
  bash \
  curl \
  dnsutils \
  git \
  python3-pip \
  hping3 \
  mariadb-client \
  mongo-tools \
  nmap \
  postgresql-client \
  redis-tools \
  tcptraceroute \
  vim \
  zsh && \
  rm -rf /var/lib/apt/lists/*

RUN pip3 install ipython

COPY zshrc /root/.zshrc
COPY vimrc /root/.vimrc
COPY vim /root/.vim

CMD ["zsh"]
